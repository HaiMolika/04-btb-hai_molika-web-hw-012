import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import Printpage1 from './Components/Printpage1';
import Home from './Components/HomeContent/Home';
import View from './Components/View';
import Video from './Components/VideoType/Video';
import Account from './Components/Accounts/Account';
import Auth from './Components/Authentication/Auth';
import NotFound from './Components/NotFound';
import CardContent from './Components/HomeContent/CardContent';
import Welcome from './Components/Authentication/Welcome';

function App() {
  return (
    <div>
        <Router>
          <View/>
          <Switch>
            <Route path='/' exact component={Printpage1}/>
            <Route path='/home' exact component={Home}/>
            <Route path='/video' exact component={Video}/>
            <Route path='/account'  component={Account}/>
            <Route path='/auth'  component={Auth}/>
            <Route path='/welcome'  component={Welcome}/>
            <Route path='/post/:id'  component={CardContent}/>
            <Route path='*' component={NotFound}/>
          </Switch>
        </Router>
    </div>
  );
}

export default App;
