import React from 'react'
import queryString from 'query-string'

function GetQuery (props){
    let accValue = queryString.parse(props.location.search);
    return (
        <div className='container'>
            {accValue.name === undefined ? 
                <h4>There is no name in the query string.</h4> :
                <h4>The <span style={{color: 'red'}}>name</span> in the query string is "{accValue.name}"</h4>     
            }
        </div>
    )
}


export default GetQuery
