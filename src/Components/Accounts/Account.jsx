import React, { Component } from 'react'
import {BrowserRouter as Router, Switch, Route,Link} from 'react-router-dom'
import GetQuery from './GetQuery'
export default class Account extends Component {
    
    render() {
        return (
            <Router>
                <div className='container'>
                    <h1>Account</h1>
                    <ul>
                        <li>
                            <Link to={'/account/?name=netflix'}>Netflix</Link>
                        </li>
                        <li>
                            <Link to={'/account/?name=zillow-group'}>Zillow Group</Link>
                        </li>
                        <li>
                            <Link to={'/account/?name=yahoo'}>Yahoo</Link>
                        </li>
                        <li>
                            <Link to={'/account/?name=modus-create'}>Modus Create</Link>
                        </li>
                    </ul>
                </div>
                <Switch>
                    <Route path='/account/' component={GetQuery}/>
                </Switch>
            </Router>
        )
    }
}
