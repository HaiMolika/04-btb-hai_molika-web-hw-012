import React, { Component } from 'react';

class CardContent extends Component {
    render() {
        return (
            <div style={{margin: '30px'}}>
                <center>
                    <p>This is the content from post {this.props.match.params.id}</p>
                </center>
            </div>
        );
    }
}

export default CardContent;