import React, { Component } from 'react'
import {Card, CardDeck, Container, Row} from 'react-bootstrap'
import {Link} from 'react-router-dom'

export default class Home extends Component {
    render() {
        return (
            <div>
                <Container>
                    <Row style={{padding: '20px'}}>
                        <CardDeck>
                            <Card>
                                <Card.Img variant="top" src="https://cdn.pixabay.com/photo/2015/12/01/20/28/road-1072823_1280.jpg" />
                                <Card.Body>
                                <Card.Title>Card title</Card.Title>
                                <Card.Text>
                                    This is a wider card with supporting text below as a natural lead-in to
                                    additional content. This content is a little bit longer.
                                </Card.Text>
                                <Card.Link as={Link} to='post/1'>Read more</Card.Link>
                                </Card.Body>
                                <Card.Footer>
                                <small className="text-muted">Last updated 3 mins ago</small>
                                </Card.Footer>
                            </Card>
                            <Card>
                                <Card.Img variant="top" src="https://cdn.pixabay.com/photo/2017/02/01/22/02/mountain-landscape-2031539_1280.jpg" />
                                <Card.Body>
                                <Card.Title>Card title</Card.Title>
                                <Card.Text>
                                    This card has supporting text below as a natural lead-in to additional
                                    content.{' '}
                                </Card.Text>
                                <Card.Link as={Link} to='post/2'>Read more</Card.Link>
                                </Card.Body>
                                <Card.Footer>
                                <small className="text-muted">Last updated 3 mins ago</small>
                                </Card.Footer>
                            </Card>
                            <Card>
                                <Card.Img variant="top" src="https://cdn.pixabay.com/photo/2015/12/01/20/28/green-1072828__340.jpg" />
                                <Card.Body>
                                <Card.Title>Card title</Card.Title>
                                <Card.Text>
                                    This is a wider card with supporting text below as a natural lead-in to
                                    additional content. This card has even longer content than the first to
                                    show that equal height action.
                                </Card.Text>
                                <Card.Link as={Link} to='post/3'>Read more</Card.Link>
                                </Card.Body>
                                <Card.Footer>
                                <small className="text-muted">Last updated 3 mins ago</small>
                                </Card.Footer>
                            </Card>
                        </CardDeck>
                    </Row>
                    <Row style={{padding: '10px 20px'}}>
                        <CardDeck>
                            <Card>
                                <Card.Img variant="top" src="https://cdn.pixabay.com/photo/2015/09/09/16/05/forest-931706__340.jpg" />
                                <Card.Body>
                                <Card.Title>Card title</Card.Title>
                                <Card.Text>
                                    This is a wider card with supporting text below as a natural lead-in to
                                    additional content. This content is a little bit longer.
                                </Card.Text>
                                <Card.Link as={Link} to='post/4'>Read more</Card.Link>
                                </Card.Body>
                                <Card.Footer>
                                <small className="text-muted">Last updated 3 mins ago</small>
                                </Card.Footer>
                            </Card>
                            <Card>
                                <Card.Img variant="top" src="https://cdn.pixabay.com/photo/2018/04/16/16/16/sunset-3325080__340.jpg" />
                                <Card.Body>
                                <Card.Title>Card title</Card.Title>
                                <Card.Text>
                                    This card has supporting text below as a natural lead-in to additional
                                    content.{' '}
                                </Card.Text>
                                <Card.Link as={Link} to='post/5'>Read more</Card.Link>
                                </Card.Body>
                                <Card.Footer>
                                <small className="text-muted">Last updated 3 mins ago</small>
                                </Card.Footer>
                            </Card>
                            <Card>
                                <Card.Img variant="top" src="https://cdn.pixabay.com/photo/2015/01/28/23/35/landscape-615429__340.jpg" />
                                <Card.Body>
                                <Card.Title>Card title</Card.Title>
                                <Card.Text>
                                    This is a wider card with supporting text below as a natural lead-in to
                                    additional content. This card has even longer content than the first to
                                    show that equal height action.
                                </Card.Text>
                                <Card.Link as={Link} to='post/6'>Read more</Card.Link>
                                </Card.Body>
                                <Card.Footer>
                                <small className="text-muted">Last updated 3 mins ago</small>
                                </Card.Footer>
                            </Card>
                        </CardDeck>
                    </Row>
                </Container>
            </div>
        )
    }
}
