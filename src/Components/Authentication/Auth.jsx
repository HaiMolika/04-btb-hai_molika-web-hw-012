import React, { Component } from 'react';
import {Form,Col, Button} from 'react-bootstrap'
import {Redirect} from 'react-router-dom'

class Auth extends Component {
    constructor(props){
        super(props);
        this.state = {
            username : null,
            password : null,
            loggedIn : false
        }
    }
    onchange= (e) => {
        console.log(e.target.value)
        this.setState({[e.target.name] : e.target.value})
    }
    onclick = (e) => {
        console.log(this.state.username)
        console.log(this.state.password)
        if(this.state.username !== null && this.state.password != null && this.state.username !=='' && this.state.password !==''){
           this.setState({ loggedIn : true}) 
        }
    }
    render() {
       if(this.state.loggedIn){
            return <Redirect to={{
                pathname : '/welcome/',
                state : {loggedIn : this.state.loggedIn }
        }}/>
       } 
            
        return (
            <div classname='container'>
                <Form>
                    <Form.Row>
                        <Col md='2'/>
                        <Col md= '3'>
                            <Form.Control placeholder="Username" name="username" onChange={this.onchange}/>
                        </Col>
                        <Col md= '3'>
                            <Form.Control type= "password" placeholder="Password" name="password" onChange={this.onchange}/>
                        </Col>
                        <Col md= '2'>
                            <Button variant="secondary" type="button" onClick= {this.onclick}>Submit</Button>
                        </Col>
                    </Form.Row>
                </Form>
            </div>
        );
    }
}

export default Auth;