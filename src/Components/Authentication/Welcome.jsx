import React, { Component } from 'react'
import {Redirect} from 'react-router-dom'

export default class Welcome extends Component {
    constructor(props){
        super(props)
        let loggedIn = props.location.state;
        if(loggedIn === undefined){
            loggedIn = false;
        }
        this.state ={
            loggedIn 
        }
    }
    render() {
        if(!this.state.loggedIn){
            return <Redirect to='/auth'/>
        }
        return (
            <div className='container'>
                <h2>Welcome</h2>
            </div>
        )
    }
}
