import React, { Component } from 'react'
import {Link, BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import SelectVideo from './SelectVideo';
import Movie from './Movie';
import Animation from './Animation';

export default class Video extends Component {

    render() {
        return (
            <Router>
                <div className='container'>
                    <ul>
                        <li>
                            <Link to={'/video/animation'}>Animation</Link>
                        </li>
                        <li>
                            <Link to={'/video/movie'}>Movie</Link>
                        </li>
                    </ul>
                    <Switch>
                        <Route path={'/video/'} exact component={SelectVideo}/>
                        <Route path={'/video/animation'}  component={Animation}/>
                        <Route path={'/video/movie'}  component={Movie}/>
                    </Switch>
                </div>
            </Router>
        )
    }
}
