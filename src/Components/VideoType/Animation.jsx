import React, { Component } from 'react'
import {Link,BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import SelectVideo from './SelectVideo'
import PrintTitle from './PrintTitle'

export default class Animation extends Component {
    render() {
        return (
            <Router>
                <div>
                    <h3>Animation Category</h3>
                    <ul>
                        <li>
                            <Link to={'/video/animation/action'}>Action</Link>
                        </li>
                        <li>
                            <Link to={'/video/animation/romance'}>Romace</Link>
                        </li>
                        <li>
                            <Link to={'/video/animation/comedy'}>Comedy</Link>
                        </li>
                    </ul>
                </div>
                <Switch>
                    <Route path='/video/animation/' exact component={SelectVideo}/>
                    <Route path='/video/animation/:videotype' component={PrintTitle}/>
                </Switch>
            </Router>
        )
    }
}
