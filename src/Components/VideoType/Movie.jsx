import React, { Component } from 'react'
import {BrowserRouter as Router, Link, Switch, Route} from 'react-router-dom'
import SelectVideo from './SelectVideo'
import PrintTitle from './PrintTitle'
export default class Movie extends Component {
    render() {
        return (
            <Router>
                <div>
                    <h3>Movie Category</h3>
                    <ul>
                        <li>
                            <Link to={'/video/movie/advanture'}>Adventure</Link>
                        </li>
                        <li>
                            <Link to={'/video/movie/comedy'}>Comedy</Link>
                        </li>
                        <li>
                            <Link to={'/video/movie/crime'}>Crime</Link>
                        </li>
                        <li>
                            <Link to={'/video/movie/documentary'}>Documentary</Link>
                        </li>
                    </ul>
                </div>
                <Switch>
                    <Route path='/video/movie' exact component={SelectVideo}/>
                    <Route path='/video/movie/:videotype' component={PrintTitle}/>
                </Switch>
            </Router>
        )
    }
}
